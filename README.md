# [FElayout](https://bitbucket.org/pixelant/felayout_ricky) **0.1.0**
Frontend framework for **TYPO3**. Part of **Pixelant Core**.

[**Contributing**](#markdown-header-contributing)

[**Changelog**](#markdown-header-changelog)

***

###Before start work with FElayout, please read [documentation](https://bitbucket.org/pixelant/felayout_ricky/wiki)

Documentation:

* [Pixelant Core](https://docs.google.com/a/pixelant.se/document/d/1gA5JR7k3WfwqxTjFcQdZu-EPBPTxWAdPe6pBC1PMH80/edit?usp=sharing)
* [FElayout](https://bitbucket.org/pixelant/felayout_ricky/wiki)
* [t3layout](https://bitbucket.org/pixelant/t3layout_ricky/wiki/)
* [pxa\_generic_content](https://bitbucket.org/pixelant/pxa_generic_content)


### Required dependencies for this repository:

* Git
* [NodeJs](http://nodejs.org/)
    - **Node Version >=0.10.25**
    - **NPM Version >=1.4.14**
* [Bower](http://bower.io/) `npm install -g bower` **Version >=1.3.8**
* [Grunt](http://gruntjs.com/) `npm install -g grunt-cli`

***

#### Git instructions

[Git workflow](https://docs.google.com/a/pixelant.se/document/d/1gVPXDCBrR66Ahhh7c6BMBeQZ0KpWWxhlLEw6zNOZX1g/edit?usp=sharing)

[Semantic Versioning](http://semver.org/)

***

# Contributing

Everyone can leave feedback, bug reports and even better - pull requests. Here is several rules for contribute this repository.  Please keep this in mind for better cooperation.

### Issues

If you have a question not covered in the documentation or want to report a bug, the best way to ensure it gets addressed is to file it in the appropriate issues tracker.

* Used the search feature to ensure that the bug hasn't been reported before
* Try to reduce your code to the bare minimum required to reproduce the issue. This makes it much easier (and much faster) to isolate and fix the issue.


### Pull requests

If you want to fix bug by yourself or add new features, you can use pull requests.

* One repository:

    1. Clone repository `git clone git@bitbucket.org:pixelant/felayout_ricky.git`
    2. Checkout to branch `dev` `git checkout -t origin/dev`
    3. Create new branch from `dev` `git checkout -b branchName`
    4. Add your changes, and commit it.
    5. Push your branch to repository `git push origin branchName`
    6. Create pull request on Bitbucket

* Fork repository:

    1. Fork repository with name `FElayout\_ricky_userName`
    2. Add changes to `dev` branch, or another, but not `master`!
    3. Create pull request to original repo `FElayout_ricky`

* Please check to make sure that there aren't existing pull requests attempting to address the issue mentioned. Also recommend checking for issues, maybe somebody working with the same issue.
* Non-trivial changes should be discussed in an issue first.
* Lint the code by running `grunt check`.
* Dont forget about [editorconfig](http://editorconfig.org/).

***

***
#Changelog
***

# **0.1.0** (20.10.2014)
* New files structure. Less partials, more content elements.
* New system how to build menu. Similar to TYPO3, added layout and hideInMenu variables.
* New 5-level menu
* New scalable header. Like we had footer in previos version of FElayout.
* New generic elements. Main menu, sub menu, breadcrumbs, language menu, menu toggle button, search form, search form toggle button.
* New templates for pages. Similar to templates on TYPO3.
* New config for icon fonts. Added selection.json and style.css from icomoon.


# **0.0.8** (22.08.2014)
* New Parallax
* Performance optimization
* Added force option to jshint and jscs code checker
* Update .gitignore

# **0.0.7** (08.08.2014)
* Added instructions for Designers
* IE fixes
* Fix Search form on startPage
* Stop news carousel
* new Parallax

# **0.0.6** (22.07.2014)
* Fixed css bugs
* Added documentation
* Update dependencies

# **0.0.5** (14.07.2014)


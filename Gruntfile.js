module.exports = function(grunt) {
    require('time-grunt')(grunt);
    require('load-grunt-config')(grunt, {
        data: {
            site: 'site',
            small: 'small',
            big: 'big',
            dev: 'dev',
            tmp: '.tmp',
            temp: 'temp',
            bc: 'dev/bower_components/',
            repo: 'git@bitbucket.org:pixelant/felayout_31173_services.git'
        }
    });
    grunt.loadNpmTasks('assemble');
};

// jquery
@import '../bower_components/jquery/dist/jquery.js';

// BOOTSTRAP
@import '../bower_components/bootstrap/js/transition.js';
@import '../bower_components/bootstrap/js/alert.js';
// @import '../bower_components/bootstrap/js/button.js';
@import '../bower_components/bootstrap/js/carousel.js';
@import '../bower_components/bootstrap/js/collapse.js';
@import '../bower_components/bootstrap/js/dropdown.js';
@import '../bower_components/bootstrap/js/modal.js';
// @import '../bower_components/bootstrap/js/tooltip.js';
// @import '../bower_components/bootstrap/js/popover.js';
// @import '../bower_components/bootstrap/js/scrollspy.js';
@import '../bower_components/bootstrap/js/tab.js';
// @import '../bower_components/bootstrap/js/affix.js';

// ISOTOPE
@import '../bower_components/isotope/dist/isotope.pkgd.js';


// FElayout
@import 'general.js';
@import 'lightbox.js';
@import 'parallax.js';
@import 'isotope.js';
@import 'custom.js';

// ########## general.js ###########

// === Enabling Active State on iOS
window.onload = function() {
    if (/iP(hone|ad)/.test(window.navigator.userAgent)) {
        var elements = document.querySelectorAll('button, a');
        var emptyFunction = function() {};
        for (var i = 0; i < elements.length; i++) {
            elements[i].addEventListener('touchstart', emptyFunction, false);
        }
    }
};

// ======= jQuery noConflict ======
// jQuery.noConflict();

// ====== class fo fixed main navigation bar   =======

jQuery(function($) {
    var navbar = $('.pxa-main-nav');
    var offsetTop = navbar.offset().top;
    $(window).on('load scroll', function() {
        var scrollPos = $(window).scrollTop();
        if (scrollPos > offsetTop) {
            $('body:not(.navbarFixed)').addClass('navbarFixed');
        } else {
            $('body.navbarFixed').removeClass('navbarFixed');
        }
    });
});

// ^^^^^^^^^^ general.js ^^^^^^^^^^^

module.exports = {
    site: {
        files: [{
            expand: true,
            cwd: '<%= tmp %>',
            src: ['images/{,*/}*.{png,jpg,gif}'],
            dest: '<%= site %>'
        }]
    },
    small: {
        files: [{
            expand: true,
            cwd: '<%= tmp %>',
            src: ['images/{,*/}*.{png,jpg,gif}'],
            dest: '<%= small %>'
        }]
    },
    big: {
        files: [{
            expand: true,
            cwd: '<%= tmp %>',
            src: ['images/{,*/}*.{png,jpg,gif}'],
            dest: '<%= big %>'
        }]
    }
};
